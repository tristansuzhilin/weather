# weather

## What are the commands

Use http://localhost:8080/weather/country?apikey=1&city=Singapore&country=sg

apikey
```
the keys are between 1 to 5 inclusive.
```
City
```
Any city that exist in openweather
```
Country
```
Use the country accordingly to the City, else it will not work.
```