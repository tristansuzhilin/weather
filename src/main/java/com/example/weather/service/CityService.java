package com.example.weather.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.weather.dao.CityDAO;
import com.example.weather.model.City;

@Service
public class CityService {
	@Autowired
	private CityDAO cityDAO;
	
	public List<City> getAllCities() {
		return cityDAO.findAll();
	}
	
	public Boolean checkCityCountry(String city, String country) {
		List<City> cityList = this.getAllCities();

		return cityList.stream().anyMatch(city1 -> (country.equalsIgnoreCase(city1.getCountry()) && city.equalsIgnoreCase(city1.getCity())));
	}
}
