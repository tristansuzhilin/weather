package com.example.weather.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.weather.dao.WeatherDAO;
import com.example.weather.model.OpenWeatherResponse;
import com.example.weather.model.Weather;

@Service
public class WeatherService {

	@Autowired
	private CityService cityService;
	
	@Autowired
	private APIKeyService apiKeyService;
	
	@Autowired
	private WeatherDAO weatherDAO;
	
	public Boolean validationForCountry(String city, String country) {		
		return cityService.checkCityCountry(city, country);
	}
	
	public Boolean validationForAPIKey(int id) {
		return apiKeyService.validationForAPIKey(id);		
	}
	
	public long addOrUpdateWeather(OpenWeatherResponse weatherResponse) {
		if(weatherResponse.getWeather().length > 0) {
			Weather weather = new Weather(weatherResponse.getWeather()[0],weatherResponse.getCity(),weatherResponse.getCountry());
			//weatherDAO.addOrUpdateWeather(weather);
			weather = weatherDAO.save(weather);
			return weather.getId();
		}
		return 0;	
	}
	
	public Weather getWeatherById (long weatherid) {
		return weatherDAO.findById(weatherid).orElse(null);		
		
	}
	
	public List<Weather> getAllWeather (){
		return weatherDAO.findAll();
	}
	

}
