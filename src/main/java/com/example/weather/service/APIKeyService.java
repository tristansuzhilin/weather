package com.example.weather.service;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.weather.dao.APIKeyDAO;
import com.example.weather.model.APIKey;


@Service
public class APIKeyService {

	@Autowired
	APIKeyDAO apiKeyDAO;
	
	public Boolean validationForAPIKey(int id) {	
		APIKey APIKeyResult = apiKeyDAO.getAPIKeyByID(id);
		
		if (APIKeyResult == null)
			return true;

		if(APIKeyResult.getLocked() && APIKeyResult.getFirstAccessTime().until(LocalDateTime.now(), ChronoUnit.HOURS) > 0) {
			apiKeyDAO.setResetAccessCountForAPIKey(id, 1, LocalDateTime.now(),false);
			return false;
		}
		this.addAccessCountForAPIKey(APIKeyResult);
		
		return APIKeyResult.getLocked();		
	}
	
	private void addAccessCountForAPIKey (APIKey APIKeyResult) {			
		System.out.println("getAccessCount: " +APIKeyResult.getAccessCount() + " Time: "+ APIKeyResult.getFirstAccessTime());
		if(APIKeyResult.getAccessCount() == 0)
			apiKeyDAO.setFirstAccessCountForAPIKey(APIKeyResult.getId(), 1,LocalDateTime.now());
		else if(APIKeyResult.getAccessCount() < 4)
			apiKeyDAO.setAccessCountForAPIKey(APIKeyResult.getId(), APIKeyResult.getAccessCount()+1);
		else
			apiKeyDAO.setMaxAccessCountForAPIKey(APIKeyResult.getId(), 5, true);
	}
}
