package com.example.weather.rest;

import java.time.LocalDateTime;

import org.apache.commons.text.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.example.weather.model.OpenWeatherResponse;
import com.example.weather.model.Weather;
import com.example.weather.service.WeatherService;


@RestController
@RequestMapping("/weather")
public class WeatherController {

	@Autowired
	RestTemplate restTemplate;
	
	@Autowired
	WeatherService weatherService;
	
	@Value("${api_key}")
	private String apikey;
	
	@GetMapping("/")
	public String sayHello() {
		return "Hello World! Time on Weather REST is " + LocalDateTime.now();
	}
	
	private OpenWeatherResponse getWeatherFromOpenWeather(String city,String country) {

        ResponseEntity<OpenWeatherResponse> response = restTemplate
                .getForEntity("https://api.openweathermap.org/data/2.5/weather?q=" + city + ","
                        + country + "&APPID=" + apikey, OpenWeatherResponse.class);
        OpenWeatherResponse result = response.getBody();
        result.setCity(city);
        result.setCountry(country);
        return result;
    }
	
	@GetMapping("/country")
	public String getWeatherByCountry(@RequestParam(value = "apikey") int apikey, @RequestParam(value = "city",defaultValue = "") String city, @RequestParam(value = "country",defaultValue = "") String country) {

		if(!weatherService.validationForCountry(WordUtils.capitalize(city),country))
			return "City or Country is invalid.";
		
		if(weatherService.validationForAPIKey(apikey))
			return "The API Key has either exceeded the rate limit of 5 weather report per hour or invalid API Key.\nPlease select another API Key from the following (1,2,3,4,5)";
		
		long weatherid = weatherService.addOrUpdateWeather(getWeatherFromOpenWeather(city, country));
		//Get weather data from h2
		Weather weather = weatherService.getWeatherById(weatherid);
		
		if (weather != null)
			return "Current Weather in "+ city +", "+ country +" is "+ weather.getDescription()+".";
		else
			return "Weather not available. Please check with admin.";
	}


};