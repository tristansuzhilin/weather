package com.example.weather.rest;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.weather.service.CityService;

@RestController
@RequestMapping("/city")
public class CityController {
	
	@Autowired
	CityService cityService;
	
	@GetMapping("/")
	public String sayHello() {
		return "Hello World! Time on City server is " + LocalDateTime.now();
	}
	
	@GetMapping("/getCities")
	public @ResponseBody ResponseEntity<Object> getAllCities() {

		return new ResponseEntity<Object>(cityService.getAllCities(), HttpStatus.OK);

	}
}
