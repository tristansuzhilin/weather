package com.example.weather.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="City")
@Table(name="CITY")
@Getter
@Setter
@ToString
@NoArgsConstructor
public class City {

	@Id
	@Column(name = "id")
	private long id;

	@Column(name = "city")
	private String city;

	@Column(name = "state")
	private String state;
	
	@Column(name = "country")
	private String country;
}
