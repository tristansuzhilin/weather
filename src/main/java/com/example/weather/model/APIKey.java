package com.example.weather.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="ApiKey")
@Table(name="APIKEY")
public class APIKey {
	@Id
	@Column(name = "id")
	private int id;
	
	@Column(name = "accessCount")
	private int accessCount;

	@Column(name = "firstAccessTime")
	private LocalDateTime firstAccessTime;
	
	@Column(name = "locked")
	private Boolean locked;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAccessCount() {
		return accessCount;
	}

	public void setAccessCount(int accessCount) {
		this.accessCount = accessCount;
	}

	public LocalDateTime getFirstAccessTime() {
		return firstAccessTime;
	}

	public void setFirstAccessTime(LocalDateTime firstAccessTime) {
		this.firstAccessTime = firstAccessTime;
	}

	public Boolean getLocked() {
		return locked;
	}

	public void setLocked(Boolean locked) {
		this.locked = locked;
	}

	@Override
	public String toString() {
		return "APIKey [id=" + id + ", accessCount=" + accessCount + ", firstAccessTime="
				+ firstAccessTime + ", locked=" + locked + "]";
	}
	
	
}
