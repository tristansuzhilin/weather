package com.example.weather.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name="Weather")
@Table(name="WEATHER")
@JsonIgnoreProperties(ignoreUnknown = true)
@Getter
@Setter
@ToString
@NoArgsConstructor
public class Weather {

	@Id
	@JsonProperty("id")
	@Column(name = "id")
	private long id;

	@JsonProperty("main")
	@Column(name = "main")
	private String main;

	@JsonProperty("description")
	@Column(name = "description")
	private String description;
	
	@Column(name = "city")
	private String city;
	
	@Column(name = "country")
	private String country;

	public Weather(long id, String main, String description) {
		super();
		this.id = id;
		this.main = main;
		this.description = description;
	}
	
	public Weather(Weather weatherResponse, String city, String country) {
		super();
		this.id = weatherResponse.getId();
		this.main = weatherResponse.getMain();
		this.description = weatherResponse.getDescription();
		this.city = city;
		this.country = country;
	}
}
