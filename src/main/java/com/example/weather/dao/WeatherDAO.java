package com.example.weather.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.weather.model.Weather;

@Repository
public interface WeatherDAO extends JpaRepository<Weather,Long> {

}