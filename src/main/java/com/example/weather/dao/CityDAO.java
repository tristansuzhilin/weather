package com.example.weather.dao;

import java.util.List;

import com.example.weather.model.City;
import com.example.weather.model.Weather;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityDAO extends JpaRepository<City,Long> {

}