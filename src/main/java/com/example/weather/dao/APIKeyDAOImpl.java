package com.example.weather.dao;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.weather.model.APIKey;

@Repository
public class APIKeyDAOImpl implements APIKeyDAO {

	@PersistenceContext
    private EntityManager entityManager;
	
	public APIKey getAPIKeyByID(int id) {
		// TODO Auto-generated method stub
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query<APIKey> theQuery = currentSession.createQuery("from ApiKey e WHERE e.id = :id",APIKey.class);
		
		List<APIKey> APIKeyResult = theQuery.setParameter("id", id).getResultList();
		
		if (APIKeyResult.isEmpty()) {
	        return null; //or throw checked exception data not found
	    } else {
	        return APIKeyResult.get(0);
	    }
	}
	
	@Transactional
	public int setFirstAccessCountForAPIKey(int id, int accessCount, LocalDateTime dateTime) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query<?> theQuery = currentSession.createQuery("UPDATE ApiKey ak SET ak.accessCount = :accessCount, ak.firstAccessTime = :dateTime WHERE ak.id = :id");
		
		return theQuery.setParameter("accessCount", accessCount).setParameter("id", id).setParameter("dateTime", dateTime).executeUpdate();
	}

	@Transactional
	public int setAccessCountForAPIKey(int id, int accessCount) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query<?> theQuery = currentSession.createQuery("UPDATE ApiKey SET access_count = :accessCount WHERE id = :id");
		
		return theQuery.setParameter("accessCount", accessCount).setParameter("id", id).executeUpdate();
	}

	@Transactional
	public int setMaxAccessCountForAPIKey(int id, int accessCount, Boolean locked) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query<?> theQuery = currentSession.createQuery("UPDATE ApiKey SET access_count = :accessCount, locked = :locked WHERE id = :id");

		return theQuery.setParameter("accessCount", accessCount).setParameter("id", id).setParameter("locked", locked).executeUpdate();
	}


	@Transactional
	public int setResetAccessCountForAPIKey(int id, int accessCount, LocalDateTime dateTime, Boolean locked) {
		Session currentSession = entityManager.unwrap(Session.class);
		
		Query<?> theQuery = currentSession.createQuery("UPDATE ApiKey SET accessCount = :accessCount, firstAccessTime = :dateTime, locked = :locked WHERE id = :id");

		return theQuery.setParameter("accessCount", accessCount).setParameter("dateTime", dateTime).setParameter("locked", locked).setParameter("id", id).executeUpdate();
	}


	
	


}
