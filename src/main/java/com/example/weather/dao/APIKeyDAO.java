package com.example.weather.dao;

import java.time.LocalDateTime;

import com.example.weather.model.APIKey;

public interface APIKeyDAO {
	public APIKey getAPIKeyByID(int id);
	public int setFirstAccessCountForAPIKey(int id, int accessCount,LocalDateTime dateTime);
	public int setAccessCountForAPIKey(int id, int accessCount);
	public int setMaxAccessCountForAPIKey(int id, int accessCount,Boolean locked);
	public int setResetAccessCountForAPIKey(int id, int accessCount, LocalDateTime dateTime, Boolean locked);
}